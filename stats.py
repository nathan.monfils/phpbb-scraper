#!/usr/bin/env python3


import matplotlib.pyplot as plt

import os
import sys
import re
import sqlite3
import datetime
import dateutil.parser

def get_first_date(conn):
    cur = conn.cursor()
    cur.execute('SELECT date FROM messages ORDER BY date ASC LIMIT 1')
    for row in cur:
        return dateutil.parser.parse(row[0])

    return None

def make_x_axis(first_date, last_date, steps):
    step = (last_date.timestamp() - first_date.timestamp()) / float(steps)
    x = [0] * steps
    for i in range(0, steps):
        x[i] = datetime.datetime.fromtimestamp(first_date.timestamp() + step * i)

    return x

def messages_f_time(conn, domain_name):
    N_BARS = 100
    first_date = get_first_date(conn)
    last_date = datetime.datetime.now()

    dates = []
    cur = conn.cursor()
    cur.execute('SELECT date FROM messages')
    for row in cur:
        dates.append(dateutil.parser.parse(row[0]))

    y = [0] * N_BARS
    x = make_x_axis(first_date, last_date, N_BARS)
    for date in dates:
        for i in range(0, N_BARS):
            if date.timestamp() <= x[i].timestamp():
                y[i] = y[i] + 1
                break

    plt.bar(x, y, N_BARS / 8, color='red')
    plt.title('Number of messages as a function of time')
    plt.xlabel('Time')
    plt.ylabel('Number of messages')

    os.makedirs('images/' + domain_name + '_stats', exist_ok=True)
    plt.savefig('images/' + domain_name + '_stats/messages.png', format='png')
    plt.cla()

def users_f_time(conn, domain_name):
    first_date = get_first_date(conn)
    last_date = datetime.datetime.now()

    dates = []
    cur = conn.cursor()
    cur.execute('SELECT joined FROM users')
    for row in cur:
        dates.append(dateutil.parser.parse(row[0]))

    N_STEPS = 1000
    x = make_x_axis(first_date, last_date, N_STEPS)
    y = [0] * N_STEPS
    for date in dates:
        for i in range(0, N_STEPS):
            if date.timestamp() <= x[i].timestamp():
                y[i] = y[i] + 1

    plt.plot(x, y)
    plt.title('Number of users as a function of time')
    plt.xlabel('Time')
    plt.ylabel('Number of users')

    os.makedirs('images/' + domain_name + '_stats', exist_ok=True)
    plt.savefig('images/' + domain_name + '_stats/users.png', format='png')
    plt.cla()

def print_longest_threads(conn):
    cur = conn.cursor()
    cur.execute('''
        SELECT threads.id, (SELECT count(messages.id) FROM messages WHERE messages.thread = threads.id), messages.date, messages.title, users.name FROM threads
        JOIN messages ON messages.id = threads.first_message
        JOIN users ON users.id=messages.author
        ORDER BY (SELECT count(messages.id) FROM messages WHERE messages.thread = threads.id) DESC LIMIT 10
    ''')
    for row in cur:
        print(str(row[0]) + ': ' + str(row[1]) + ' messages, posted on ' + row[2] + '. Thread: "' + row[3] + '" by ' + row[4])

def most_common_words(conn, domain_name):
    words = {}

    cur = conn.cursor()
    cur.execute('SELECT messages.content FROM messages')
    for row in cur:
        split = row[0].split()
        for word in split:
            word = re.sub('[,\\.<>\\(\\)\\[\\]\\{\\}\\*\\+\\\\\\/\'"!@#$%&\\-=\\?\\:\\|]', '', word).lower().replace('usergenerated', '') # 'usergenerated' is very frequent because of image alt-texts, but is ironically not user-generated!
            if word != '':
                #print(word)
                if word in words:
                    words[word] = words[word] + 1
                else:
                    words[word] = 1

    x = []
    y = []
    words_ordered = sorted(words.items(), key=lambda tup: tup[1], reverse=True)[:50] # Only keep the first 50 words for memory reasons
    for word in words_ordered:
        x.append(word[0])
        y.append(word[1])

    plt.figure(num=None, figsize=(16, 8), dpi=80)
    plt.bar(x, y)
    plt.title('Most frequent words on the forum')
    #plt.xlabel('Words')
    plt.ylabel('Occurences')
    plt.xticks(rotation=-45)
    os.makedirs('images/' + domain_name + '_stats', exist_ok=True)
    plt.savefig('images/' + domain_name + '_stats/most_frequent_words.png', format='png')
    plt.cla()


def run():
    print('[WARNING] Make sure you\'ve already ran fix_dates.py!')

    if len(sys.argv) != 2:
        print('Usage: ./stats.py <domain name>')
        exit()
    domain_name = sys.argv[1]
    if not os.path.isfile(domain_name + '.sqlite'):
        print('Invalid domain name! No \'' + domain_name + '.sqlite\' file.')
        exit()

    conn = sqlite3.connect(domain_name + '.sqlite')
    cur = conn.cursor()

    messages_f_time(conn, domain_name)
    users_f_time(conn, domain_name)
    #print_longest_threads(conn)
    most_common_words(conn, domain_name)

    conn.close()

run()