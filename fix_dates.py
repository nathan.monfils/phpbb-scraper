#!/usr/bin/env python3

import sys
import re
import sqlite3

MONTHS = {
    'Jan': 1,
    'Fév': 2,
    'Mar': 3,
    'Avr': 4,
    'Mai': 5,
    'Juin': 6,
    'Juil': 7,
    'Aoû': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Déc': 12,
}

print('This program will only work with the specific date format used on the forum I developped this script')
print('With dates such as: \'le Sam 13 Avr 2013 - 13:49\' (for posts) and \'13/04/2013\' (for users)')


if len(sys.argv) != 2:
    print('Usage: ./fix_dates.py <sqlite file>')
    exit()
file = sys.argv[1]

p = re.compile('^.*le [A-Za-z]+ ([0-9]+) ([A-Za-zéû]+) ([0-9]+) \\- ([0-9]+):([0-9]+)$')

fill = '{num:02d}'
fillyear = '{num:04d}'

conn = sqlite3.connect(file)
to_fix = []
cur = conn.cursor()
cur.execute('SELECT id, date FROM messages')
for row in cur:
    m = p.match(row[1])
    if m is None or len(m.groups()) != 5:
        #print('Could not parse date:', row[1])
        continue

    day = int(m.groups()[0])
    month = MONTHS[m.groups()[1]]
    year = int(m.groups()[2])
    hour = int(m.groups()[3])
    minute = int(m.groups()[4])

    fill = '{num:02d}'
    formatted = fillyear.format(num=year) + '-' + fill.format(num=month) + '-' + fill.format(num=day) + 'T' + fill.format(num=hour) + ':' + fill.format(num=minute)
    to_fix.append((row[0], formatted))

for msg in to_fix:
    cur.execute('UPDATE messages SET date=? WHERE id=?', (msg[1], msg[0]))

conn.commit()



p = re.compile('^([0-9]+)/([0-9]+)/([0-9]+)$')

birth_to_fix = []
joined_to_fix = []
cur = conn.cursor()
cur.execute('SELECT id, birth, joined FROM users')
for row in cur:
    m1 = p.match(row[1])
    m2 = p.match(row[2])
    if m1 != None and len(m1.groups()) == 3:
        formatted = fillyear.format(num=int(m1.groups()[2])) + '-' + fill.format(num=int(m1.groups()[1])) + '-' + fill.format(num=int(m1.groups()[0]))

        birth_to_fix.append((row[0], formatted))

    if m2 != None and len(m2.groups()) == 3:
        formatted = fillyear.format(num=int(m2.groups()[2])) + '-' + fill.format(num=int(m2.groups()[1])) + '-' + fill.format(num=int(m2.groups()[0]))

        joined_to_fix.append((row[0], formatted))

for msg in birth_to_fix:
    cur.execute('UPDATE users SET birth=? WHERE id=?', (msg[1], msg[0]))

for msg in joined_to_fix:
    cur.execute('UPDATE users SET joined=? WHERE id=?', (msg[1], msg[0]))

conn.commit()

conn.close()
