import urllib.request
import os
import sys
import hashlib

def get(url):
    print('GET ' + url)
    f = urllib.request.urlopen(url)
    return f.read().decode('utf-8')

def downloadImage(domain_name, url):
    print('GET ' + url)

    extension = ''
    id = hashlib.sha1(url.encode()).hexdigest()

    if len(url) < 5: # No valid URL is this short, and this prevents a crash when checking the extension
        return None

    if url[len(url) - 4] == '.':
        extension = url[len(url) - 4:]

    dir = 'images/' + domain_name
    file = dir + '/' + id + extension

    if os.path.isfile(file):
        print('File already downloaded, skipping...')
        return file

    try:
        req = urllib.request.Request(url, data=None, headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        })
        f = urllib.request.urlopen(req, timeout=2)
    except:
        print('Could not get image:', sys.exc_info())
        return None
    if f.getcode() is None: # For some reason phpbb supports embedded base-64 encoded images, which do not have a return code??
        return None
    if f.getcode() / 100 != 2 and f.getcode() / 100 != 3:
        print('HTTP Status ' + str(f.getcode()))
        return None


    os.makedirs(dir, exist_ok=True)
    try:
        open(file, 'wb').write(f.read())
    except:
        os.remove(file)
        print('Could not save file')
        return None

    return file
