class Post:
    def __init__(self):
        self.title = ''
        self.author = ''
        self.date = ''
        self.content = ''

    def save(self, thread, cur):
        """Saves the message to the database and returns its ID"""
        cur.execute('''
            INSERT INTO messages(thread, title, author, content, date) VALUES(?, ?, ?, ?, ?)
        ''', (thread, self.title, self.author, self.content, self.date))

        return cur.lastrowid

    def __str__(self):
        return 'Title: ' + self.title + '\nAuthor:' + self.author + '\nDate' + self.date + '\nContent:' + self.content
