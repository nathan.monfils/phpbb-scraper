from bs4 import BeautifulSoup
from bs4 import NavigableString

import webutils
import forum
from forum import Forum

def parse_description(div):
    mod_text = div.select_one('mod-text')
    if mod_text is None:
        return ''
    if mod_text.previous_sibling != None and mod_text.previous_sibling.previous_sibling != None \
        and mod_text.previous_sibling.previous_sibling.name != None:
        return mod_text.previous_sibling.previous_sibling.text
    return ''

def get_parent(domain_name, title):
    if not title.has_attr('href'):
        return -1

    url = 'http://' + domain_name + title['href']
    html = webutils.get(url)
    soup = BeautifulSoup(html, 'html.parser')

    forums = soup.select('.topic-actions .path a')
    if len(forums) < 3:
        return -1
    else:
        parent = forums[len(forums) - 2]
        if not parent.has_attr('href'):
            return -1
        return forum.parseForumPath(parent['href'])

class ForumList:
    def __init__(self, domain_name, html):

        soup = BeautifulSoup(html, 'html.parser')
        self.forums = []

        for title in soup.select('.forumtitle'):
            f = Forum()
            f.id = forum.parseForumPath(title['href'])
            f.title = title.string
            f.description = parse_description(title.parent.parent)
            f.parent = get_parent(domain_name, title)

            self.forums.append(f)

    def save(self, conn):
        """Save the user to the DB and creates a corresponding thread"""

        cur = conn.cursor()
        for forum in self.forums:
            forum.save(cur)
        conn.commit()

    def __str__(self):
        out = ''
        for forum in self.forums:
            out = out + str(forum) + '\n\n~~~~~~~~~~~~\n\n'

        return out
